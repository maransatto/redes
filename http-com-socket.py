# Criar um servidor HTTP com o socket
import socket
from threading import Thread

server_socket = socket.socket(
    socket.AF_INET, socket.SOCK_STREAM,
)
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

server_socket.bind(('0.0.0.0', 8000))

server_socket.listen(1)

def accept_http(client_address, client_socket):
    print(client_address, client_socket)
    data = client_socket.recv(1024)
    content = '''
    <h1>The earths is flat!</h1>
    '''
    reponse = '''
    HTTP/1.1 200 OK\r\nContent-Type: text/html;\r\ncharset=UTF-8\r\nContent-Length: %d

    %s
    ''' % (len(content), content)
    client_socket.send(response)

    print(data)

while True:
    client_address, client_socket = serversocket.accept()
    thread = Thread(target=accept_http, args=(client_address, client_socket))
    thread.start()