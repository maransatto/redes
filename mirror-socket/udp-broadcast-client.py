import socket
import json
# import thread

cs = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
cs.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
cs.bind(('', 37020))

address = '<broadcast>'
port = 37020

ips =	{
  "10.64.243.16": "Maransatto",
  "10.64.241.182": "Álvaro",
  "10.64.243.27": "Samuel",
  "10.64.255.178": "Tales",
  "10.64.243.121": "Sulfite",
  "10.64.241.89": "Pedro",
  "10.64.246.117": "Ettore",
  "10.64.242.248": "Brito"

}

def send_id(name):
    pacote = dict(type="id", data=name)
    raw_pack = json.dumps(pacote).encode()
    socket.socket.send(raw_pack, (address, port))

# def send_msg():
#     pacote = dict(type='msg', data=name)
#     raw_pack = json.dumps(pacote).encode()
#     socket.socket.send(raw_pack, (address, port))

# def receive():
#     cs.bind(('', port))
# 
#     while True:
#         data, address = cs.recvfrom(1024)
#         print(address, data)


while True:

    data, addr = cs.recvfrom(1024)
    try:
        remetente = ips[addr[0]]
    except:
        remetente = addr[0]

    print("%s: %s"%(remetente,data.decode()))