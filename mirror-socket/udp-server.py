import socket


HOST = ''
PORT = 5000

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind((HOST, PORT))

while True:
    msg, client = s.recvfrom(1024)
    print(msg.upper(), client)
    s.sendto(msg.upper(), client)

s.close()