from socket import *
import time


server = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)
server.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)

server.settimeout(0.2)
server.bind(('', 44444))

# mensagem = 'Brito bobão, Renã Cabeção'

server.sendto('...... Maransatto entrou ............'.encode(), ('<broadcast>', 37020))

while True:

    print('Enviar:')
    mensagem = input()

    server.sendto(mensagem.encode(), ('<broadcast>', 37020))
    print('mensagem enviada!')
    time.sleep(1)